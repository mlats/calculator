package com.example.calculatorapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var firstvariable: Double= 0.0
    private var secondvriable:Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init()
    {
        n0Button.setOnClickListener(this)
        n1Button.setOnClickListener(this)
        n2Button.setOnClickListener(this)
        n3Button.setOnClickListener(this)
        n4Button.setOnClickListener(this)
        n5Button.setOnClickListener(this)
        n6Button.setOnClickListener(this)
        n7Button.setOnClickListener(this)
        n8Button.setOnClickListener(this)
        n9Button.setOnClickListener(this)
        backspaceButton.setOnLongClickListener {
            resultTextView.text = ""
            firstvariable=0.0
            secondvriable=0.0
            operation=""
            true
        }
        dotButton.setOnClickListener{
            fun dot (view: View)
            {
                val value =resultTextView.text.toString()
                if(value.isNotEmpty())
                {
                    resultTextView.text=resultTextView.toString()+"."
                    dotButton.isClickable=false
                }
            }
        }
    }
    fun equal (view: View)
    {
        val value=resultTextView.text.toString()
        if(value.isNotEmpty() && operation.isNotEmpty()) {
            secondvriable = value.toDouble()
            var result: Double = 0.0
            if (operation == ":") {
                n0Button.isClickable=false
                result = firstvariable / secondvriable
            }
            if (operation=="-")
            {
                result=firstvariable-secondvriable
            }
            if (operation == "+")
            {
                result = firstvariable+secondvriable
            }
            if (operation == "x")
            {
                result=firstvariable*secondvriable
            }
            if (operation == ".")
            {

            }
            resultTextView.text = result.toString()
            operation = ""
            firstvariable=0.0
            secondvriable=0.0
            n0Button.isClickable=true
            dotButton.isClickable=true
        }
    }

    fun delete(view: View)
    {
        val value = resultTextView.text.toString()
        if(value.isNotEmpty()){ resultTextView.text=value.substring(0,value.length-1)}
    }
    fun substract (view: View)
    {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
        {
            firstvariable=value.toDouble()
            operation="-"
            resultTextView.text=""
        }
    }
    fun add (view: View)
    {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
        {
            firstvariable=value.toDouble()
            operation="+"
            resultTextView.text=""
        }
    }
    fun multiply (view: View)
    {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
        {
            firstvariable=value.toDouble()
            operation="x"
            resultTextView.text=""
        }
    }
    fun divide (view: View)
    {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty())
        {
            firstvariable=value.toDouble()
            operation=":"
            resultTextView.text=""
        }
    }
    override fun onClick(v: View?)
    {
        val button = v as Button
        resultTextView.text=resultTextView.text.toString()+button.text.toString()
    }

}